## Folder api_calls ##
This folder contains the API collection directly or not related to the Job Alert journey. 

To call out them you need to set up postman environment with these parameters:

# Postman Environemnt Key/Pair and how to get each #
* **definitionId** - result from an API response 
* **client_id** - from Job Alert package at Marketing Cloud setup
* **client_secret** -  - from Job Alert package at Marketing Cloud setup
* **jobAlertAccessToken** - result from an API response 
* **soap_user_name** - from Salesforce Integration user at Marketing Cloud setup (or Application Credentials spreadsheet)
* **soap_passwd** - from Salesforce Integration user at Marketing Cloud setup (or Application Credentials spreadsheet)
* **mc_instance** - from Job Alert package at Marketing Cloud setup