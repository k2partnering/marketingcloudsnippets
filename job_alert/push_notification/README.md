## Content ##

Push notifications are sent by the journey to every matched UserID. In production it only sends push notification of Recommended Job type.

![Job Alert Push Notification](job-alert-push-notification.png)


## Development and Testing ##

In order to test this in sandbox, we prepared the Job Alert Push Test journey which sends two different push notification:

![Job Alert Push Test Journey](job-alert-push-test-journey.png)


**Recommended Job**
![Recommended Job](testing-push-notification-recommended-jobs.png)


**Job Page**
![Job Page](testing-push-notification-job-page.png)

