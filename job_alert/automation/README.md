## REMARK ##

Please use the CloudPageForDebug.html to validate SSJS. At that moment, you want to change the code and publish and check the script on the preview panel.
More info at https://k2partnering.atlassian.net/browse/KKCA-5638


## Content ##
There are two automations regarding Job Alert initiative

* The Job Alert Email Metrics contains bounces and not-sent events out from the journey history.
* The Job Alert Journey contains the ETL of ingested data from GCP that ultimately triggers the Job Alert journey