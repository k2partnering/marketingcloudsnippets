/*
Name: Job Alert Email Bounce
External Key: job-alert-email-bounce
Folder Location: Query
Description: Email bounces from Job Alert only
Data Extension: Job Alert Metrics
Data Action: Append
*/
select 
su.SubscriberKey as "ContactID",
su.EmailAddress as "EmailAddress",
jah.Source as "Source",
j.JourneyName as "JourneyName",
j.VersionNumber as "VersionNumber",
jah.SyncId as "SyncID",
s.JobID,
s.ListID,
s.BatchID,
b.SMTPBounceReason as "ErrorCode",
'Bounce' as "Category",
ja.JourneyActivityObjectID as "ActivityID",
convert(varchar, getutcdate(), 126) as "CreatedDate",
convert(varchar, s.EventDate, 126) as "TriggeredAt"
from [_Sent] s
join [_JourneyActivity] ja 
on s.TriggererSendDefinitionObjectID = ja.JourneyActivityObjectID
join [_Journey] j
on ja.VersionID = j.VersionID
left join [_Bounce] b
on s.JobID = b.JobID
and s.ListID = b.ListID
and s.BatchID = b.BatchID
and s.SubscriberID = b.SubscriberID
join [_Subscribers] su
on s.SubscriberID = su.SubscriberID
left join [Job Alert History] jah
on su.SubscriberKey = substring(jah.ContactId, 0, patindex('%-%', jah.ContactId))
where ja.ActivityType in ('EMAIL','EMAILV2')
and s.EventDate <= cast(cast(dateadd(hh,-73,getutcdate()) as date) as datetime)
and s.EventDate > cast(cast(dateadd(hh,-72,getutcdate()) as date) as datetime)
and j.JourneyName = 'Job Alert Journey'
and b.SubscriberID is not null



/*
Name: Complement journey history data
External Key: job-alert-complement-journey-history
Folder Location: Query
Description: This complements the journey history by adding contact, job and other information.
Data Extension: Job Alert Metrics
Data Action: Update
*/
select jam.ContactID, sbs.EmailAddress as EmailAddress, jah.Source as Source, jam.JourneyName, 
jny.VersionNumber as VersionNumber, jah.SyncID, s.JobID, s.ListID, s.BatchID, jam.TriggeredAt as TriggeredAt,
jam.ErrorCode, jam.Category, jam.CreatedDate, jam.ActivityID
from [Job Alert Metrics] jam
join _Subscribers sbs
on jam.ContactID = sbs.SubscriberKey
join [Job Alert History] jah
on sbs.SubscriberKey = substring(jah.ContactId, 0, patindex('%-%', jah.ContactId))
join [_JourneyActivity] jnya 
on jnya.ActivityID = jam.ActivityID
join [_Journey] jny
on jnya.VersionID = jny.VersionID
left join [_Sent] s
on jam.ActivityID = s.TriggererSendDefinitionObjectID