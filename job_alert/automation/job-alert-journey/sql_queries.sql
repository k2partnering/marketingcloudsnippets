/*
Name: Copy input DE from prod
External Key: job-alert-copy-input-de-prod
Folder Location: Query
Description: Copy input data to minimise data loss (prod input)
Data Extension:Job Alert Temporary DE
Data Action:Append
*/
select ContactId, ContactName, UserId, ProjectRole, PreferencesUrl, Source, ImportedAt, SyncId
from "Job Alert DE"
where left(ContactId, charindex('-', ContactId) - 1) not in (
        '0033C00000It2dXQAR', '0053C000003ELPMQA4',
        '0031M00003ABpcKQAT', '0053C000003TWuDQAW'
    )


/*
Name: Copy input DE from sandbox
External Key: job-alert-copy-input-de-sandbox
Folder Location: Query
Description: Copy input data to minimise data loss (sandbox input)
Data Extension: Job Alert Temporary DE
Data Action: Append
*/
select ContactId, ContactName, UserId, ProjectRole, PreferencesUrl, Source, ImportedAt, SyncId
from "Job Alert DE Sandbox"
where left(ContactId, charindex('-', ContactId) - 1) in (
        '0033C00000It2dXQAR', '0053C000003ELPMQA4',
        '0031M00003ABpcKQAT', '0053C000003TWuDQAW'
    )


/*
Name: Delete all Job Alert DE records
External Key: job-alert-delete-all-de-rows
Folder Location:Query
Description: Delete all Job Alert DE records
Data Extension: Job Alert DE
Data Action: Overwrite
*/
select
ContactId, ContactName, UserId, ProjectRole, PreferencesUrl, Source, ImportedAt
from "Job Alert DE"
WHERE 1=0


/*
Name: Delete all Job Alert DE Sandbox records
External Key: job-alert-delete-all-de-rows-sandbox
Folder Location: Query
Description: Delete all Job Alert DE Sandbox records
Data Extension: Job Alert DE Sandbox
Data Action: Overwrite
*/
select
ContactId, ContactName, UserId, ProjectRole, PreferencesUrl, Source, ImportedAt
from "Job Alert DE Sandbox"
WHERE 1=0


/*
Name: Append to Job Alert history
External Key: job-alert-append-history
Folder Location: Query
Description: Append new entries to create a historical data extension for further analysis.
Data Extension: Job Alert History
Data Action: Append
*/
select ContactId+'-'+convert(varchar, getutcdate(), 120) as ContactId, 
ContactName, UserId, ProjectRole, PreferencesUrl, Source, ImportedAt, SyncId
from "Job Alert Temporary DE"


/*
Name: Split rows into contact and user from Job Alert Sent
External Key: job-alert-split-contact-user
Folder Location: Query
Description: Split rows into contact and user. ContactID links to SF data to email contacts.
Data Extension: Job Alert Sent
Data Action: Append
*/
select 
UserId as ContactId, ContactName, null as UserId, ProjectRole, PreferencesUrl, Source, ProjectRoleTitle, Location, EmailTemplate
from "Job Alert Sent"


/*
Name: Append to Job Alert Sent History
External Key: job-alert-sent-append-history
Folder Location: Query
Description: Create a backup of the dataset to be sent via journey
Data Extension: Job Alert Sent History
Data Action: Append
*/
select ContactId+'-'+convert(varchar, getutcdate(), 120) as ContactId, 
ContactName, UserId, ProjectRole, PreferencesUrl, Source, ProjectRoleTitle, Location, 
convert(varchar, getutcdate(), 126) as EnteredJourneyAt, EmailTemplate
from "Job Alert Sent"


/*
Name: Delete all Job Alert Sent records
External Key: job-alert-sent-delete-all-rows
Folder Location: Query
Description: Delete all rows from Job Alert Sent
Data Extension: Job Alert Sent
Data Action: Overwrite
*/
select
ContactId, ContactName, UserId, ProjectRole, PreferencesUrl, Source
from "Job Alert Sent"
WHERE 1=0


/*
Name: Delete all Temp DE records
External Key: job-alert-delete-all-temp-de-rows
Folder Location: Query
Description: Delete all rows of Job Alert Temporary DE
Data Extension: Job Alert Temporary DE
Data Action: Overwrite
*/
select
ContactId, ContactName, UserId, ProjectRole, PreferencesUrl, Source, ImportedAt
from "Job Alert Temporary DE"
WHERE 1=0

/*
Name: 
External Key: 
Folder Location: 
Description: 
Data Extension: 
Data Action: 
*/