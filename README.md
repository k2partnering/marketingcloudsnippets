# README #

Marketing Cloud Code Snippets

### What is this repository for? ###

* This repo stores scripts created for Marketing Cloud activities. 

### Q1 2021 - Job Alert Journey ###

* Motivation https://k2partnering.atlassian.net/wiki/spaces/CHAT/pages/675119105/Job+Alerts+Journey
* BPMNs https://k2partnering.atlassian.net/wiki/spaces/CHAT/pages/679247874/Job+Alerts 

### Who do I talk to? ###

* Email me at ltetsuo@k2partnering.com

![Job Alert Journey](job_alert/job-alert-journey.png)